import 'package:flutter/material.dart';

/*
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
*/
class AddNotesPage extends StatefulWidget {
  const AddNotesPage({Key? key}) : super(key: key);

  @override
  State<AddNotesPage> createState() => _AddNotesPageState();
}

class _AddNotesPageState extends State<AddNotesPage> {
  int currentIndex = 0;
  dynamic fieldController = TextEditingController();
  String fieldContent = "";
  DateTime date = DateTime(2022, 01, 01);

  show(index) {
    if (index == 0) {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Albums"),
                  const SizedBox(
                    height: 15.0,
                  ),
                  TextButton(
                      onPressed: () {}, child: const Text("Prendre une photo")),
                  const SizedBox(
                    height: 15.0,
                  ),
                  TextButton(
                      onPressed: () {},
                      child:
                          const Text("Choisissez une photo parmi les albums"))
                ],
              ));
    } else if (index == 2) {
      return () async {
        DateTime? date = await showDatePicker(
            context: context,
            initialDate: DateTime(2022, 05, 01),
            firstDate: DateTime(1900),
            lastDate: DateTime(2100));
      };
    }
  }

  @override
  void initState() {
    super.initState();
    fieldController.addListener(
        () => setState(() => {fieldContent = fieldController.value.text}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Note"),
        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 8.0, top: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "23 juin, 2022 15:33",
              ),
              const SizedBox(
                height: 30.0,
              ),
              const Text(
                "Titre",
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                ),
              ),
              const SizedBox(
                height: 15.0,
              ),
              TextFormField(
                controller: fieldController,
                minLines: 5,
                maxLines: 50,
                keyboardType: TextInputType.multiline,
                decoration: const InputDecoration(
                    hintText: 'Prendre des notes',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: InputBorder.none),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentIndex,
          backgroundColor: Colors.deepOrange,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white70,
          iconSize: 30.0,
          onTap: (index) {
            (index) => setState(() {
                  currentIndex = index;
                });
            show(index);
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.image),
              label: 'Album',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.task),
              label: 'Liste des tâches',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: 'Rappel',
            )
          ],
        ));
  }
}

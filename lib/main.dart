import 'package:flutter/material.dart';
import 'package:mynotesbloc/pages/add_notes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const FirstPage(),
    );
  }
}

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          const SizedBox(
            height: 80.0,
          ),
          Row(
            children: const [
              SizedBox(
                width: 20.0,
              ),
              Text(
                "Bloc-notes",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  fontSize: 35.0,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20.0,
          ),
          Center(child: Image.asset("assets/addNotes.png")),
          const SizedBox(
            height: 10.0,
          ),
          const Center(
            child: Text(
              "Appuyer sur + pour créer une note ",
              style: TextStyle(
                color: Colors.black,
                fontSize: 17.0,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AddNotesPage()),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
